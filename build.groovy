def build()
{
    try
	{
		stage('building...')
		{
			sh (script: "npm run deploy")
		}
	}
	catch (ex)
	{
	    throw ex
	}
}

return this;
