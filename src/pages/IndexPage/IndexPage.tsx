import React from 'react';
import { Theme, AppBar, Toolbar, Button } from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/styles';
import { width } from '@material-ui/system';

export interface IndexPageProps {
    title?: string;
}

const useStyle = makeStyles(
    (theme: Theme) => createStyles(
        {
            toolbar: theme.mixins.toolbar,
            wrapper: {
                backgroundColor: theme.palette.secondary.main,
                width: 100,
                height: 120
            }
        }
    ), 
{name: 'IndexPage'});

const IndexPage: React.FC<IndexPageProps> = ({ title, children }) => {
    const classes = useStyle();

    return (
        <React.Fragment>
            <AppBar>
                <Toolbar>{title}</Toolbar>
            </AppBar>
            <div className={classes.toolbar} />
            <div className={classes.wrapper} />
            {children}
        </React.Fragment>
    );
}

export default IndexPage;