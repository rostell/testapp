import React from 'react';
import useReactRouter from 'use-react-router';
import { Button } from '@material-ui/core';
import TableContainer from '../../containers/TableContainer';

const TablePage: React.FC = () => {
    const { location, history, match } = useReactRouter();

    const clickHandler = () => {
        history.goBack();
    };

    return (
        <React.Fragment>
            <Button onClick={clickHandler}>Back</Button>
            <TableContainer />
        </React.Fragment>
    )
}

export default TablePage;