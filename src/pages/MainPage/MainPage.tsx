import React from 'react';
import useReactRouter from 'use-react-router';
import { Button } from '@material-ui/core';

const MainPage: React.FC = () => {
    const { location, history, match } = useReactRouter();

    const clickHandler = () => {
        history.push("/table");
    };

    return <Button onClick={clickHandler}>Go table</Button>
}

export default MainPage;