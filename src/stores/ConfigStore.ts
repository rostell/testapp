import { types, flow, Instance } from 'mobx-state-tree';
import ConfigModel from './ConfigModel';

const ConfigItems = types.array(ConfigModel);

export type IConfigItems = Instance<typeof ConfigItems>;

const ConfigStore = types.model({
    items: ConfigItems
}).actions(self => ({
    fill: (items: IConfigItems) => {
        self.items = items;
    },
    getList: flow(function* getList() {
        const resp = yield fetch('http://192.168.0.12/api/crud/v1/user/read?td=test.okteller.ru&login=admin&pwd=admin');

        const body = yield resp.json();

        console.log(body);

        self.items = body.data.map((item: { id: string; name: string; login: string}) => ({ ...item, state: item.login }));
    }),
})).views(self => ({
    getByID: (id: string) => {
        return self.items.find(item => item.id === id);
    }
}));

export default ConfigStore;