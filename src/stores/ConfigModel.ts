import { types } from 'mobx-state-tree';

const ConfigModel = types.model({
    id: types.string,
    name: types.string,
    state: types.string,
});

export default ConfigModel;