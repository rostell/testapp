import React, { useContext } from 'react';
import { types } from 'mobx-state-tree';
import { useLocalStore } from 'mobx-react-lite';
import ConfigStore from './ConfigStore';

const Store = types.model({
    configStore: ConfigStore
});

export const StoreContext = React.createContext<typeof Store.Type | undefined>(undefined);

export const StoreProvider: React.FC = ({ children }) => {
    const store = useLocalStore(createStore);
    return <StoreContext.Provider value={store}>{children}</StoreContext.Provider>
};

export const createStore = () => Store.create({
    configStore: {}
});

export const useStore = () => useContext(StoreContext);