import React, { useCallback, useEffect } from 'react';
import { Button } from '@material-ui/core';
import { useStore } from '../../stores';
import Table from '../../components/Table';
import { observer } from 'mobx-react-lite';
import { IConfigItems } from '../../stores/ConfigStore';

const TableContainer = observer(() => {
    const store = useStore();

    useEffect(() => {
        store && store.configStore.getList();
    }, []);

    const fillHandle = useCallback(() => {
        // store && store.configStore.fill([{id: '1', name: 'test', state: 'active'}] as IConfigItems);
        store && store.configStore.getList();
    }, [store]);

    return (
        <React.Fragment>
            <Button onClick={fillHandle}>Fill</Button>
            {store && <Table rows={store.configStore.items.toJS()} />}
        </React.Fragment>
    );
})

export default TableContainer;