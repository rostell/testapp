import React from 'react';
import logo from './logo.svg';
import './App.css';
import { IndexPage, MainPage, TablePage } from './pages';
import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { StoreContext, createStore, StoreProvider } from './stores';
import purple from '@material-ui/core/colors/purple';

const theme = createMuiTheme({
  palette: {
    secondary: purple
  }
});

const App: React.FC = () => {
  return (
    <StoreProvider>
      <ThemeProvider theme={theme}>
        <IndexPage title="Title">
          <Router>
            <Switch>
              <Route path="/main" component={MainPage} />
              <Route path="/table" component={TablePage} />
              <Redirect to="/main" />
            </Switch>
          </Router>
        </IndexPage>
      </ThemeProvider>
    </StoreProvider>
  );
}

export default App;